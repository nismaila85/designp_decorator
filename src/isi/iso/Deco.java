package isi.iso;

public abstract class Deco implements Notification {
	 
   
    protected Notification notificationdeco;
 
   
    public Deco(Notification notificationdeco)
    {
       
        this.notificationdeco = notificationdeco;
    }
 
  
    public void notifier() { notificationdeco.notifier(); }
}